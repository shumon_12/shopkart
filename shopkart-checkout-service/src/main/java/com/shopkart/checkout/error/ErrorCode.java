package com.shopkart.checkout.error;

import java.util.Date;

public class ErrorCode {

	private ErrorCodeTypes errorType;
	public Date eventDate;
	private String message;

	public ErrorCode(ErrorCodeTypes errorCodeTypes, String message) {
		this.errorType = errorCodeTypes;
		this.eventDate = new Date();
		this.message = message;
	}

	public ErrorCodeTypes getErrorType() {
		return errorType;
	}

	public void setErrorType(ErrorCodeTypes errorTypes) {
		this.errorType = errorTypes;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
