package com.shopkart.checkout.error;

public class ProductNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public ProductNotFoundException() {
		super("No Product Found");
	}
}
