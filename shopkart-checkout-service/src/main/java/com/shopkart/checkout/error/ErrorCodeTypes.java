package com.shopkart.checkout.error;

public enum ErrorCodeTypes {
	BUSINESS_ERROR("Business Error"), APPLICATION_ERROR("Application Error"), UNKNOWN_ERROR("Unexpected Error");

	private ErrorCodeTypes(String errorType) {

	}
}
