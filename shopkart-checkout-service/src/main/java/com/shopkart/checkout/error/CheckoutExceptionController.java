package com.shopkart.checkout.error;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice	
public class CheckoutExceptionController {

	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseEntity<Object> handleException(ProductNotFoundException exception) {
		return new ResponseEntity<>(new ErrorCode(ErrorCodeTypes.BUSINESS_ERROR, "No Product Found"),
				HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(CartIsEmptyException.class)
	public ResponseEntity<Object> handleException(CartIsEmptyException exception) {
		return new ResponseEntity<>(new ErrorCode(ErrorCodeTypes.BUSINESS_ERROR, "No products in cart"),
				HttpStatus.NOT_FOUND);
	}

}
