package com.shopkart.checkout.service;
import com.shopkart.checkout.model.Product;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public interface ProductInformationService {

	public List<Product> getAllProductDetails();
	
	
}
