package com.shopkart.checkout.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.shopkart.checkout.controller.PaymentService;
import com.shopkart.checkout.error.CartIsEmptyException;
import com.shopkart.checkout.error.ProductNotFoundException;
import com.shopkart.checkout.model.CardTypes;
import com.shopkart.checkout.model.Cart;
import com.shopkart.checkout.model.CartSummary;
import com.shopkart.checkout.model.Product;
import com.shopkart.checkout.repository.CartRepository;
import com.shopkart.checkout.repository.ProductRepository;

@Service
@Transactional
public class CheckoutServiceImpl implements ProductInformationService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private CartRepository cartRepository;

	@Autowired
	private PaymentService paymentService;

	private List<Cart> cartList;

	private List<Product> products;

	public Product getProductDetailsByProductId(int productCode) {
		Product product = productRepository.findProductByProductCode(productCode)
				.orElseThrow(() -> new ProductNotFoundException());
		return product;
	}

	@Override
	public List<Product> getAllProductDetails() {
		products = new ArrayList<Product>();
		productRepository.findAll().forEach(prod -> products.add(prod));
		if (products.isEmpty())
			throw new ProductNotFoundException();
		return products;
	}

	public void saveOrUpdateCart(int productCode) {
		Product prod = productRepository.findProductByProductCode(productCode)
				.orElseThrow(() -> new ProductNotFoundException());
		Cart cart = new Cart();
		cart.setProductName(prod.getProductName());
		cart.setProductPrice(prod.getProductPrice());
		cart.setProductCode(prod.getProductCode());
		cartRepository.save(cart);
	}

	public void deleteFromCart(int productCode) {
		Product prod = productRepository.findProductByProductCode(productCode)
				.orElseThrow(() -> new ProductNotFoundException());
		checkIfCartIsEmpty();
		cartRepository.deleteAllByProductCode(prod.getProductCode());
	}

	public List<Cart> getAllProductsInCart() {
		checkIfCartIsEmpty();
		return cartList;
	}

	public CartSummary getDiscountPrecalculation(CardTypes card) {
		checkIfCartIsEmpty();
		return paymentService.getDiscountPrecalculation(card, cartList);
	}

	private boolean checkIfCartIsEmpty() {
		cartList = new ArrayList<Cart>();
		cartRepository.findAll().forEach(cartItem -> cartList.add(cartItem));
		if (cartList.isEmpty())
			throw new CartIsEmptyException();
		return true;
	}
}
