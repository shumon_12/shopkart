package com.shopkart.checkout.model;

import java.math.BigDecimal;

public class CartSummary {
	private BigDecimal actualPrice;
	private int applicableDiscountPercent;
	private BigDecimal discountedPrice;
	
	public BigDecimal getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(BigDecimal actualPrice) {
		this.actualPrice = actualPrice;
	}
	public BigDecimal getDiscountedPrice() {
		return discountedPrice;
	}
	public void setDiscountedPrice(BigDecimal discountedPrice) {
		this.discountedPrice = discountedPrice;
	}
	public int getApplicableDiscountPercent() {
		return applicableDiscountPercent;
	}
	public void setApplicableDiscountPercent(int applicableDiscountPercent) {
		this.applicableDiscountPercent = applicableDiscountPercent;
	}
	
	
}
