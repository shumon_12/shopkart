package com.shopkart.checkout.controller;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.shopkart.checkout.model.CardTypes;
import com.shopkart.checkout.model.Cart;
import com.shopkart.checkout.model.CartSummary;

@FeignClient("payment-service")
@RequestMapping("/api/payment")
public interface PaymentService {

	@PostMapping("/checkout/{cardtype}")
	public CartSummary getDiscountPrecalculation(@PathVariable("cardtype") CardTypes cardType,
			@RequestBody List<Cart> cartItems);

}
