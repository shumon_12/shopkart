package com.shopkart.checkout.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.shopkart.checkout.model.CardTypes;
import com.shopkart.checkout.model.Cart;
import com.shopkart.checkout.model.CartSummary;
import com.shopkart.checkout.model.Product;
import com.shopkart.checkout.service.CheckoutServiceImpl;

@RestController
@RequestMapping("/api/products")
public class CheckoutController implements ProductInformationController {

	@Autowired
	private CheckoutServiceImpl checkoutServiceImpl;

	@Override
	@GetMapping
	public List<Product> getAllMarketProducts() {
		return checkoutServiceImpl.getAllProductDetails();
	}

	@Override
	@GetMapping("/product/{productid}")
	public Product getProductByProductId(@PathVariable("productid") int productCode) {
		return checkoutServiceImpl.getProductDetailsByProductId(productCode);
	}

	@GetMapping("/cart")
	public List<Cart> getAllProductsInCart() {
		return checkoutServiceImpl.getAllProductsInCart();
	}

	@PutMapping("/cart/{productid}")
	public ResponseEntity<?> addToCart(@PathVariable("productid") int productCode) {
		checkoutServiceImpl.saveOrUpdateCart(productCode);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@DeleteMapping("/cart/{productid}")
	public ResponseEntity<?> removeFromCart(@PathVariable("productid") int productCode) {
		checkoutServiceImpl.deleteFromCart(productCode);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/cart/checkout/{cardtype}")
	public CartSummary totalCartPrice(@PathVariable("cardtype") CardTypes card) {
		return checkoutServiceImpl.getDiscountPrecalculation(card);
	}

}
