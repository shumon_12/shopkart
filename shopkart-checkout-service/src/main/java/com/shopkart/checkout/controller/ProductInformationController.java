package com.shopkart.checkout.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.shopkart.checkout.model.Product;

@RestController
@RequestMapping("/api/products/")
public interface ProductInformationController {

	public List<Product> getAllMarketProducts();
	@RequestMapping("/product/{productId}")
	public Product getProductByProductId(int productCode);

}
