package com.shopkart.checkout.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.shopkart.checkout.model.Cart;

@Repository
@Transactional
public interface CartRepository extends CrudRepository<Cart, Integer>{

	public void deleteAllByProductCode(int productCode);
}
