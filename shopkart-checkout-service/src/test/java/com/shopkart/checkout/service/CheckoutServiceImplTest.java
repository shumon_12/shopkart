package com.shopkart.checkout.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.shopkart.checkout.controller.PaymentService;
import com.shopkart.checkout.error.CartIsEmptyException;
import com.shopkart.checkout.error.ProductNotFoundException;
import com.shopkart.checkout.model.CardTypes;
import com.shopkart.checkout.model.Cart;
import com.shopkart.checkout.model.CartSummary;
import com.shopkart.checkout.model.Product;
import com.shopkart.checkout.repository.CartRepository;
import com.shopkart.checkout.repository.ProductRepository;

@RunWith(MockitoJUnitRunner.class)
public class CheckoutServiceImplTest {

	@InjectMocks
	private CheckoutServiceImpl checkoutServiceImpl;
	@Mock
	private ProductRepository productRepository;
	@Mock
	private CartRepository cartRepository;
	@Mock
	private PaymentService paymentService;

	@Test
	public void getProductDetailsByProductIdTest() {
		when(productRepository.findProductByProductCode(5)).thenReturn(getStubbedProduct());
		assertEquals(checkoutServiceImpl.getProductDetailsByProductId(5).getProductName(),
				getStubbedProduct().get().getProductName());
	}

	@Test(expected = ProductNotFoundException.class)
	public void getProductDetailsByProductIdTestThrowsException() {
		when(productRepository.findProductByProductCode(6)).thenThrow(new ProductNotFoundException());
		checkoutServiceImpl.getProductDetailsByProductId(6).getProductName();
	}

	@Test
	public void getAllProductDetailsTest() {
		when(productRepository.findAll()).thenReturn(getListOfStubbedProduct());
		assertEquals(checkoutServiceImpl.getAllProductDetails().get(1).getProductName(),
				getListOfStubbedProduct().get(1).getProductName());
	}

	@Test
	public void saveOrUpdateCartTest() {
		when(productRepository.findProductByProductCode(5)).thenReturn(getStubbedProduct());
		checkoutServiceImpl.saveOrUpdateCart(5);
		verify(cartRepository, times(1)).save(getStubbedCart());
	}

	@Test
	public void deleteFromCartTest() {
		when(productRepository.findProductByProductCode(5)).thenReturn(getStubbedProduct());
		when(cartRepository.findAll()).thenReturn(getStubbedCartList());
		checkoutServiceImpl.deleteFromCart(5);
		verify(cartRepository, times(1)).deleteAllByProductCode(5);
	}

	@Test(expected = CartIsEmptyException.class)
	public void deleteFromCartTestThrowsException() {
		when(productRepository.findProductByProductCode(5)).thenReturn(getStubbedProduct());
		when(cartRepository.findAll()).thenReturn(new ArrayList<Cart>());
		checkoutServiceImpl.deleteFromCart(5);
		verify(cartRepository, times(1)).deleteAllByProductCode(5);
	}

	@Test
	public void getAllProductsInCartTest() {
		when(cartRepository.findAll()).thenReturn(getStubbedCartList());
		assertEquals(checkoutServiceImpl.getAllProductsInCart().get(1).getProductName(), "Sample2");
	}

	@Test
	public void getDiscountPrecalculationTest() {
		when(cartRepository.findAll()).thenReturn(getStubbedCartList());
		when(paymentService.getDiscountPrecalculation(CardTypes.GOLD, getStubbedCartList()))
				.thenReturn(getStubbedCartSummary());
		assertEquals(checkoutServiceImpl.getDiscountPrecalculation(CardTypes.GOLD).getDiscountedPrice(),
				new BigDecimal(800.00).setScale(2, RoundingMode.CEILING));

	}

	private Optional<Product> getStubbedProduct() {
		Product product = new Product();
		product.setProductCode(5);
		product.setProductName("Sample1");
		product.setProductPrice(new BigDecimal(1000.0).setScale(2, RoundingMode.CEILING));
		product.setProductType("Demo");
		return Optional.ofNullable(product);
	}

	private List<Product> getListOfStubbedProduct() {
		List<Product> products = new ArrayList<Product>();
		Product product = new Product();
		product.setProductCode(5);
		product.setProductName("Sample1");
		product.setProductPrice(new BigDecimal(1000.0).setScale(2, RoundingMode.CEILING));
		product.setProductType("Demo");
		Product product2 = new Product();
		product.setProductCode(6);
		product.setProductName("Sample2");
		product.setProductPrice(new BigDecimal(3000.0).setScale(2, RoundingMode.CEILING));
		product.setProductType("Demo");
		products.add(product);
		products.add(product2);
		return products;
	}

	private Cart getStubbedCart() {
		Cart cart = new Cart();
		cart.setProductCode(5);
		cart.setProductName("Sample1");
		cart.setProductPrice(new BigDecimal(1000.0).setScale(2, RoundingMode.CEILING));
		return cart;
	}

	private List<Cart> getStubbedCartList() {
		List<Cart> cartList = new ArrayList<Cart>();
		Cart cart1 = new Cart();
		cart1.setProductCode(5);
		cart1.setProductName("Sample1");
		cart1.setProductPrice(new BigDecimal(1000.0).setScale(2, RoundingMode.CEILING));
		Cart cart2 = new Cart();
		cart2.setProductCode(6);
		cart2.setProductName("Sample2");
		cart2.setProductPrice(new BigDecimal(2000.0).setScale(2, RoundingMode.CEILING));
		cartList.add(cart1);
		cartList.add(cart2);
		return cartList;
	}

	private CartSummary getStubbedCartSummary() {
		CartSummary summary = new CartSummary();
		summary.setActualPrice(new BigDecimal(1000.0).setScale(2, RoundingMode.CEILING));
		summary.setApplicableDiscountPercent(20);
		summary.setDiscountedPrice(new BigDecimal(800.0).setScale(2, RoundingMode.CEILING));
		return summary;
	}
}
