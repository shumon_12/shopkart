package com.credit.payment.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.credit.payment.constants.CardTypes;
import com.credit.payment.model.CartItems;
import com.credit.payment.model.CartSummary;

@RunWith(MockitoJUnitRunner.class)
public class CreditCardPaymentServiceTest {

	@InjectMocks
	private CreditCardPaymentService cardPaymentService;
	@Spy
	private CreditCardDiscountCalculationImpl cardDiscountCalculationImpl;
	private List<CartItems> cartItems;

	@Before
	public void initializeMockData() {

		cartItems = new ArrayList<CartItems>();
		cartItems.add(new CartItems(1, "sample product 1", new BigDecimal(1000.00), 1));
		cartItems.add(new CartItems(2, "sample product 2", new BigDecimal(2000.00), 2));
	}

	@Test
	public void getDiscountPrecalculationTest() {
		CartSummary summary = cardPaymentService.getDiscountPrecalculation(cartItems, CardTypes.GOLD);
		assertEquals(summary.getDiscountedPrice(), getCartSummary().getDiscountedPrice());
	}

	@Test(expected = NullPointerException.class)
	public void getDiscountPrecalculationTestShouldThrowNullPointerException() {
		List<CartItems> cartList = new ArrayList<CartItems>();
		cartList.add(null);
		CartSummary summary = cardPaymentService.getDiscountPrecalculation(cartList, CardTypes.GOLD);
		assertEquals(summary.getDiscountedPrice(), getCartSummary().getDiscountedPrice());
	}

	public CartSummary getCartSummary() {
		return new CartSummary(new BigDecimal(3000.00).setScale(2, RoundingMode.CEILING), 20,
				new BigDecimal(2400.00).setScale(2, RoundingMode.CEILING));
	}

}
