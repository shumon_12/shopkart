package com.credit.payment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.credit.payment.constants.CardTypes;
import com.credit.payment.model.CartItems;
import com.credit.payment.model.CartSummary;

@Service
public class CreditCardPaymentService {

	@Autowired
	private CreditCardDiscountCalculationImpl cardDiscountCalculationImpl;
	
	public CartSummary getDiscountPrecalculation(List<CartItems> cartItems, CardTypes card) {
	return cardDiscountCalculationImpl.flatDiscount(cartItems, card);	
		
	}
	
	
	
}
