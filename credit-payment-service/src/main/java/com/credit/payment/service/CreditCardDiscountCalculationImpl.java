package com.credit.payment.service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import org.springframework.stereotype.Component;

import com.credit.payment.constants.CardTypes;
import com.credit.payment.model.CartItems;
import com.credit.payment.model.CartSummary;

@Component
public class CreditCardDiscountCalculationImpl implements CreditCardDiscountCalculation {

	public BigDecimal totalCartCost;
	public BigDecimal totalCostAfterDiscount;
	private static final int mathContext = 2;

	@Override
	public CartSummary flatDiscount(List<CartItems> cartItems, CardTypes card) {
		totalCartCost = totalCostOfCartProducts(cartItems);
		totalCostAfterDiscount = totalCostOfCartAfterDiscount(totalCartCost, card);
		CartSummary cartSummary = new CartSummary();
		cartSummary.setActualPrice(totalCartCost);
		cartSummary.setApplicableDiscountPercent(card.getDiscount());
		cartSummary.setDiscountedPrice(totalCostAfterDiscount);
		return cartSummary;
	}

	private BigDecimal totalCostOfCartProducts(List<CartItems> cartItems) {
		return cartItems.stream().map(cart -> cart.getProductPrice()).reduce(BigDecimal.ZERO, BigDecimal::add)
				.setScale(mathContext, RoundingMode.CEILING);
	}

	private BigDecimal totalCostOfCartAfterDiscount(BigDecimal totalCost, CardTypes card) {
		return card.getDiscount() != 0
				? totalCost.subtract(totalCost.multiply(new BigDecimal(card.getDiscount())).divide(new BigDecimal(100)))
						.setScale(mathContext, RoundingMode.CEILING)
				: totalCost;
	}

}
