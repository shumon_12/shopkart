package com.credit.payment.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.credit.payment.constants.CardTypes;
import com.credit.payment.model.CartItems;
import com.credit.payment.model.CartSummary;

@Component
public interface CreditCardDiscountCalculation {

	public CartSummary flatDiscount(List<CartItems> cartItems, CardTypes card);
	
	default public CartSummary seasonAndFlatDiscount(List<CartItems> cartItems) {
		return null;
	}
}
