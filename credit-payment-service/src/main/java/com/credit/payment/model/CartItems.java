package com.credit.payment.model;

import java.math.BigDecimal;
import java.util.Objects;

public class CartItems {

	private int items;
	private String productName;
	private BigDecimal productPrice;
	private int productCode;

	public int getItems() {
		return items;
	}

	public void setItems(int items) {
		this.items = items;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	public int getProductCode() {
		return productCode;
	}

	public void setProductCode(int productCode) {
		this.productCode = productCode;
	}

	@Override
	public String toString() {
		return "CartItems [items=" + items + ", productName=" + productName + ", productPrice=" + productPrice
				+ ", productCode=" + productCode + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(items, productCode, productName, productPrice);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CartItems other = (CartItems) obj;
		return items == other.items && productCode == other.productCode
				&& Objects.equals(productName, other.productName) && Objects.equals(productPrice, other.productPrice);
	}

	public CartItems(int items, String productName, BigDecimal productPrice, int productCode) {
		super();
		this.items = items;
		this.productName = productName;
		this.productPrice = productPrice;
		this.productCode = productCode;
	}

	public CartItems() {

	}
}
