package com.credit.payment.model;

import java.math.BigDecimal;

public class CartSummary {
	private BigDecimal actualPrice;
	private int applicableDiscountPercent;
	private BigDecimal discountedPrice;

	public BigDecimal getActualPrice() {
		return actualPrice;
	}

	public void setActualPrice(BigDecimal actualPrice) {
		this.actualPrice = actualPrice;
	}

	public int getApplicableDiscountPercent() {
		return applicableDiscountPercent;
	}

	public void setApplicableDiscountPercent(int applicableDiscountPercent) {
		this.applicableDiscountPercent = applicableDiscountPercent;
	}

	public BigDecimal getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(BigDecimal discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	public CartSummary(BigDecimal actualPrice, int applicableDiscountPercent, BigDecimal discountedPrice) {
		super();
		System.out.println(actualPrice);
		this.actualPrice = actualPrice;
		System.out.println(this.actualPrice);
		this.applicableDiscountPercent = applicableDiscountPercent;
		this.discountedPrice = discountedPrice;
	}

	public CartSummary() {

	}
}
