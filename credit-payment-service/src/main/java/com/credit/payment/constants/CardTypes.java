package com.credit.payment.constants;

public enum CardTypes {
	GOLD(20), SILVER(10), NORMAL(0);

	private int discount;

	private CardTypes(int discount) {
		this.discount = discount;
	}

	public int getDiscount() {
		return discount;
	}

}
