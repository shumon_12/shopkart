package com.credit.payment.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.credit.payment.constants.CardTypes;
import com.credit.payment.model.CartItems;
import com.credit.payment.model.CartSummary;

@RestController
@RequestMapping("/api/payment/")
public interface PaymentController {
	@PostMapping("/checkout/{cardtype}/")
	public CartSummary getDiscountPrecalculation(@PathVariable("cardtype") CardTypes cardType, 
			@RequestBody List<CartItems> paymentRequest);
}
