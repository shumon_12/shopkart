package com.credit.payment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.credit.payment.constants.CardTypes;
import com.credit.payment.model.CartItems;
import com.credit.payment.model.CartSummary;
import com.credit.payment.service.CreditCardPaymentService;

@RestController
@RequestMapping("/api/payment")
public class CreditCardPaymentController implements PaymentController {

	@Autowired
	private CreditCardPaymentService cardPaymentService;
	
	@Override
	@PostMapping(value = "/checkout/{cardtype}")
	public CartSummary getDiscountPrecalculation(@PathVariable("cardtype") CardTypes cardType, 
			@RequestBody List<CartItems> cartItems) {
		return cardPaymentService.getDiscountPrecalculation(cartItems, cardType);
	}

}
